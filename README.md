# Arduinocrat #

Physical game/doll for Global Game Jam 2017.

https://globalgamejam.org/2017/games/arduinocrat

## Gameplay ##
You are a maid. Your job is to take care of your master, a demanding aristrocrat.

The doll swings through various mood stages and you are supposed to find some way to please it.

It will complain about various issues it has with it's surroundings like for example temperature and light brigtness etc.

## Hardware ##

- Arduino Uno
- Sparkfun Weather Shield