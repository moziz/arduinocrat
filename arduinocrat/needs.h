typedef enum
{
  none,
  sleep,    // Light low
  warmth,   // Temp high
  coldness, // Temp low
  song,     // Sound high
  lullaby,  // Light low, sound high
  secret,   // Sound high, humidity high 	
  love		// Light low, temp high, sound high
  // If more are added, be sure to update the pickRandomNeed function!
} Need;

typedef enum
{
  content,  // Everything is fine for now
  irritated, // About to start wanting something
  need,     // Wants something
  calming  // Need has been fulfilled, calming down
} MoodPhase;

Need pickRandomNeed()
{
  long index = random(7);
  return (Need)index;
}
