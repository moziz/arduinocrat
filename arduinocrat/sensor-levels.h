// Sensor calibration
typedef struct
{
  float low;
  float high;
} RangeF;

typedef struct
{
	long int low;
	long int high;
} RangeL;

typedef struct
{
  RangeF humidity;
  RangeF light;
  RangeF pressure;
  RangeF temperature;
  RangeF sound;
} SensorLimits;

typedef struct
{
	float humidity;
	float light;
	float pressure;
	float temperature;
	float sound;
} SensorValues;

typedef struct
{
	
} SensorStatus;

bool isHigh(float value, RangeF *range)
{
  return range->high < value;
}

bool isHigh(long int value, RangeL *range)
{
  return range->high < value;
}

bool isLow(float value, RangeF *range)
{
  return range->low > value;
}

bool isLow(long int value, RangeL *range)
{
  return range->low > value;
}

bool isNeutral(float value, RangeF *range)
{
  return !isHigh(value, range) && !isLow(value, range);
}

bool isNeutral(long int value, RangeL *range)
{
  return !isHigh(value, range) && !isLow(value, range);
}

SensorLimits sensorLimits =
{
	{ 10.0f, 40.0f }, // Humidity
	{ 0.6f, 2.6f }, // Light
	{ 101000.0f, 102000.0f }, // Pressure
	{ 22.0f, 28.0f }, // Temperature
	{ 0.1f, 0.8f }, // Sound
};

SensorValues sensorValues =
{
	0.0f,
	0.0f,
	0.0f,
	0.0f,
	0.0f
};
