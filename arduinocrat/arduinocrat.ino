#include <SparkFunMPL3115A2.h>
#include <SparkFunHTU21D.h>
#include <Wire.h> //I2C needed for sensors
#include <SPI.h>
#include <TFT.h>

#include "timer.h"
#include "sensor-levels.h"
#include "needs.h"

//Hardware pin definitions
//-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
const byte EYE_NEED = 3;
const byte EYE_CONTENT = 2;
const byte REFERENCE_3V3 = A3;
const byte LIGHT = A1;
const byte BATT = A2;

MPL3115A2 myPressure; //Create an instance of the pressure sensor
HTU21D myHumidity; //Create an instance of the humidity sensor
TFT tft = TFT(10, 9, 8);

Timer checkTimer;
Need currentNeed = Need::none;

MoodPhase currentMood = MoodPhase::content;
Timer moodPhaseTimer;
bool initMoodPhase = true;
Timer eyeTimer;
bool eyeFlipFlop = true;

bool flipFlop = true;
int textLines = 0;

int buttonPin = 6;

void updateSensors()
{
  pinMode(buttonPin, INPUT_PULLUP);
  if (isTimedout(&checkTimer))
  {
    //Check Humidity Sensor
    float humidity = myHumidity.readHumidity();

    if (humidity == 998) //Humidty sensor failed to respond
    {
      Serial.println("I2C communication to sensors is not working. Check solder connections.");

      //Try re-initializing the I2C comm and the sensors
      myPressure.begin();
      myPressure.setModeBarometer();
      myPressure.setOversampleRate(7);
      myPressure.enableEventFlags();
      myHumidity.begin();
    }
    else
    {
      printSensorStatus();
    }
    
    resetTimer(&checkTimer);
  }
  
  sensorValues.humidity = myHumidity.readHumidity();
  sensorValues.light = get_light_level();
  sensorValues.pressure = myPressure.readPressure();
  sensorValues.temperature = myHumidity.readTemperature();

  if(digitalRead(buttonPin) == LOW)
  {
    sensorValues.sound = 1.0f;
  } 
  else
  {
    sensorValues.sound = 0.0f; 
  }
};

void printSensorStatus()
{
  Serial.println();
  
  Serial.print("Humidity = ");
  Serial.print(sensorValues.humidity);

  Serial.print("; Light = ");
  Serial.print(sensorValues.light);
  
  Serial.print("; Pressure = ");
  Serial.print(sensorValues.pressure);
  
  Serial.print("; Temp = ");
  Serial.print(sensorValues.temperature, 2);

  Serial.print("; Sound = ");
  Serial.print(sensorValues.sound);
  
  Serial.println();
}

void printToScreen(const char *str, int lines = 1)
{
  tft.text(str, 0, (textLines + lines - 1) * 16);
  textLines += lines;
}

//Returns the voltage of the light sensor based on the 3.3V rail
//This allows us to ignore what VCC might be (an Arduino plugged into USB has VCC of 4.5 to 5.2V)
float get_light_level()
{
  float operatingVoltage = analogRead(REFERENCE_3V3);
  float lightSensor = analogRead(LIGHT);
  operatingVoltage = 3.3 / operatingVoltage; //The reference voltage is 3.3V
  lightSensor = operatingVoltage * lightSensor;
  return (lightSensor);
}

//Returns the voltage of the raw pin based on the 3.3V rail
//This allows us to ignore what VCC might be (an Arduino plugged into USB has VCC of 4.5 to 5.2V)
//Battery level is connected to the RAW pin on Arduino and is fed through two 5% resistors:
//3.9K on the high side (R1), and 1K on the low side (R2)
float get_battery_level()
{
  float operatingVoltage = analogRead(REFERENCE_3V3);
  float rawVoltage = analogRead(BATT);
  operatingVoltage = 3.30 / operatingVoltage; //The reference voltage is 3.3V
  rawVoltage = operatingVoltage * rawVoltage; //Convert the 0 to 1023 int to actual voltage on BATT pin
  rawVoltage *= 4.90; //(3.9k+1k)/1k - multiple BATT voltage by the voltage divider to get actual system voltage
  return (rawVoltage);
}

void setup()
{
  Serial.begin(115200);
  Serial.println("Weather Shield Example");

  pinMode(EYE_NEED, OUTPUT);
  pinMode(EYE_CONTENT, OUTPUT);

  pinMode(REFERENCE_3V3, INPUT);
  pinMode(LIGHT, INPUT);

  //Configure the pressure sensor
  myPressure.begin(); // Get sensor online
  myPressure.setModeBarometer(); // Measure pressure in Pascals from 20 to 110 kPa
  myPressure.setOversampleRate(7); // Set Oversample to the recommended 128
  myPressure.enableEventFlags(); // Enable all three pressure and temp event flags

  //Configure the humidity sensor
  myHumidity.begin();
  
  tft.begin();
  tft.background(96, 0, 255);
  tft.stroke(255, 255, 255);
  tft.setTextSize(1);
  
  randomSeed((long int)(get_light_level()*1498 + get_battery_level() * 17483 + myPressure.readPressure() / 10398));

  currentNeed = pickRandomNeed();
  
  startTimer(&moodPhaseTimer, 0);
  startTimer(&eyeTimer, 250);

  Serial.println("Weather Shield online!");
}

bool isNeedFulfilled(SensorValues *vals, SensorLimits *limits, Need need)
{
  bool fulfilled = false;
  
  if (need == Need::none)
  {
    printToScreen("I need nothing");
    fulfilled = true;
  }
  else if (need == Need::sleep)
  {
    printToScreen("I want to sleep.");
    
    if (!isLow(vals->light, &limits->light)) {
      printToScreen("It's too bright!");
      Serial.print(vals->light);
    }
    else
    {
      fulfilled = true;
    }
  }
  else if (need == Need::warmth)
  {
    printToScreen("I'm sweating.");
    
    if (!isHigh(vals->temperature, &limits->temperature)) {
      printToScreen("It's too hot in here!");
    }
    else
    {
      fulfilled = true;
    }
  }
  else if (need == Need::coldness)
  {
    printToScreen("I'm sweating.");
    
    if (!isLow(vals->temperature, &limits->temperature)) {
      printToScreen("It's too hot in here!");
    }
    else
    {
      fulfilled = true;
    }
  }
  else if (need == Need::song)
  {
    printToScreen("I'm bored. Sing me a song.");
    
    if (!isHigh(vals->sound, &limits->sound)) {
      printToScreen("I can't hear you!");
    }
    else
    {
      fulfilled = true;
    }
  }
  else if (need == Need::lullaby)
  {
    printToScreen("Put me to sleep\nwith a lullaby.", 2);
    
    if (!isLow(vals->light, &limits->light)) {
      printToScreen("It's too bright!");
    }
    else if (!isHigh(vals->sound, &limits->sound))
    {
      printToScreen("I can't hear you!");
    }
    else
    {
      fulfilled = true;
    }
  }
  else if (need == Need::secret)
  {
    printToScreen("Tell me a secret.");
    
    if (!isLow(vals->light, &limits->light)) {
      printToScreen("We shan't be seen!");
    }
    else if (!isHigh(vals->sound, &limits->sound))
    {
      printToScreen("I can't hear you!");
    }
    else if (!isHigh(vals->humidity, &limits->humidity))
    {
      printToScreen("Come closer!");
    }
    else
    {
      fulfilled = true;
    }
  }
  else if (need == Need::love)
  {
    printToScreen("Love me.");
    
    if (!isLow(vals->light, &limits->light)) {
      printToScreen("I like it dim and moody!");
    }
    else if (!isHigh(vals->temperature, &limits->temperature))
    {
      printToScreen("I want to feel your heat!");
    }
    else if (!isHigh(vals->sound, &limits->sound))
    {
      printToScreen("Tell me you love me!");
    }
    else
    {
      fulfilled = true;
    }
  }
  
  return fulfilled;
}

void flashEyes()
{
  if (!isTimedout(&eyeTimer))
  {
    return;
  }
  
  eyeFlipFlop = !eyeFlipFlop;
  resetTimer(&eyeTimer);
}

void loop()
{
  updateTime();
  updateSensors();
  textLines = 0; // Reset screen line counter

  /*
  Serial.print("\ntimeNow: ");
  Serial.print(timeNow);
  Serial.print("; deltaTime: ");
  Serial.print(deltaTime);
  */
  
  // Visualize current state
  digitalWrite(EYE_NEED, eyeFlipFlop ? HIGH : LOW);
  digitalWrite(EYE_CONTENT, !eyeFlipFlop ? HIGH : LOW);
  tft.background(0,0,0);
  
  /*
  Serial.print("\nMood phase: ");
  Serial.print(currentMood);
  Serial.print("; need: ");
  Serial.print(currentNeed);
  Serial.print("\n");
  */
  
  // Mood state machine
  bool moodTimerTimedout = isTimedout(&moodPhaseTimer);
  if (currentMood == MoodPhase::content)
  {
    if (initMoodPhase)
    {
      initMoodPhase = false;
      startTimer(&moodPhaseTimer, 10000);
      eyeFlipFlop = true;
    }
    else if (moodTimerTimedout)
    {
      currentMood = MoodPhase::irritated;
      initMoodPhase = true;
    }
    else
    {
      printToScreen("I tolerate your presense.");
    }
  }
  else if (currentMood == MoodPhase::irritated)
  {
    if (initMoodPhase)
    {
      initMoodPhase = false;
      startTimer(&moodPhaseTimer, 3000);
      eyeFlipFlop = false;
    }
    else if (moodTimerTimedout)
    {
      currentMood = MoodPhase::need;
      initMoodPhase = true;
    }
    else
    {
      printToScreen("What is this?\nInfuriating!", 2);
      flashEyes();
    }
  }
  else if (currentMood == MoodPhase::need)
  {
    if (initMoodPhase)
    {
      initMoodPhase = false;
      eyeFlipFlop = false;
      currentNeed = pickRandomNeed();
    }
    else if (isNeedFulfilled(&sensorValues, &sensorLimits, currentNeed))
    {
      currentMood = MoodPhase::calming;
      initMoodPhase = true;
    }
    else
    {
      
    }
  }
  else if (currentMood == MoodPhase::calming)
  {
    if (initMoodPhase)
    {
      initMoodPhase = false;
      startTimer(&moodPhaseTimer, 3000);
      eyeFlipFlop = true;
    }
    else if (moodTimerTimedout)
    {
      currentMood = MoodPhase::content;
      initMoodPhase = true;
    }
    else
    {
      printToScreen("I guess this\nwill do for now.", 2);
      flashEyes();
    }
  }
}
