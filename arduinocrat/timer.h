long timeNow;
long deltaTime;

typedef struct {
  long beginAt;
  long duration;
} Timer;

void updateTime()
{
  long now = millis();
  deltaTime = now - timeNow;
  timeNow = now;
}

void startTimer(Timer *timer, long duration)
{
  timer->beginAt = timeNow;
  timer->duration = duration;
}

void resetTimer(Timer *timer)
{
  timer->beginAt = timeNow;
}

bool isTimedout(Timer *timer)
{
  return timeNow >= timer->beginAt + timer->duration;
}
